package ch.bbw.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ch.bbw.addressbook.Address;
import ch.bbw.addressbook.AwesomeComparator;

public class TestComparator {

	private List<Address> addresses;
	
	@Before
	public void setup() {		
		addresses = new ArrayList<>();
		addresses.add(new Address(1, "Petra", "Muster", "000 000 00 00", new Date(), "asdf@gmail.com"));
		addresses.add(new Address(2, "Peter", "Muster", "000 000 00 00", new Date(), "qwre@gmail.com"));
		addresses.add(new Address(3, "Fritz", "Müller", "000 000 00 00", new Date(), "yxcv@gmail.com"));
		addresses.add(new Address(4, "Dario", "Andres", "000 000 00 00", new Date(), "1234@gmail.com"));	
	}
	
	@Test
	public void testSort() {
		Comparator<Address> comparator = new AwesomeComparator();
		assertEquals(0, comparator.compare(addresses.get(0), addresses.get(0)));
	}
	
	@Test
	public void testSort2() {
		Comparator<Address> comparator = new AwesomeComparator();
		assertNotEquals(0, comparator.compare(addresses.get(1), addresses.get(0)));
	}
}
