package ch.bbw.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ch.bbw.addressbook.Address;
import ch.bbw.addressbook.AddressDAO;


public class AddressDAO_Mockup implements AddressDAO {	
	private List<Address> addresses;

	public AddressDAO_Mockup(Boolean addDummyAddresses) {		
		addresses = new ArrayList<>();
		if(addDummyAddresses){
			addresses.add(new Address(1, "Petra", "Muster", "000 000 00 00", new Date(), "asdf@gmail.com"));
			addresses.add(new Address(2, "Peter", "Muster", "000 000 00 00", new Date(), "qwre@gmail.com"));
			addresses.add(new Address(3, "Fritz", "Müller", "000 000 00 00", new Date(), "yxcv@gmail.com"));
			addresses.add(new Address(4, "Dario", "Andres", "000 000 00 00", new Date(), "1234@gmail.com"));	
		}
	}
	
	public void create(Address address) {
		addresses.add(address);
	}
	
	public Address read(int id) {
		return addresses.get(id-1);
	}
	public List<Address> readAll() {
		return addresses;
	}

	public void update(Address address) {
		addresses.remove(address.getId() - 1);
		addresses.add(address);
	}
		
	public void delete(int id) {
		addresses.remove(id);
	}
}