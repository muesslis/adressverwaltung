package ch.bbw.test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ch.bbw.addressbook.Address;
import ch.bbw.addressbook.AddressDAO_Memory;
import ch.bbw.addressbook.AddressService;
import ch.bbw.addressbook.AddressValidator;

public class TestAddress {
	Address address = null;
	
	@Test
	public void testAddressWithNormalPhoneNumber() {
		Address address = new Address(1, "Peter", "Muster", "0000000000", "peter.muster@gmail.com");
		assertEquals(address.getId(), 1);
		assertEquals(address.getFirstname(), "Peter");
		assertEquals(address.getLastname(), "Muster");
		assertEquals(address.getPhonenumber(), "0000000000");
		assertNull(address.getRegistrationDate());
		assertEquals(address.getEmail(), "peter.muster@gmail.com");
	}
	
	@Test
	public void testAddressWithSpacedPhoneNumber() {
		Address address = new Address(1, "Peter", "Muster", "000 000 00 00", "peter.muster@gmail.com");
		assertEquals(address.getId(), 1);
		assertEquals(address.getFirstname(), "Peter");
		assertEquals(address.getLastname(), "Muster");
		assertEquals(address.getPhonenumber(), "000 000 00 00");
		assertNull(address.getRegistrationDate());
		assertEquals(address.getEmail(), "peter.muster@gmail.com");
	}
	
	@Test
	public void testAddressWithNormalPhoneNumberAndDate() {
		Date date = new Date();
		Address address = new Address(1, "Peter", "Muster", "0000000000", date, "peter.muster@gmail.com");
		assertEquals(address.getId(), 1);
		assertEquals(address.getFirstname(), "Peter");
		assertEquals(address.getLastname(), "Muster");
		assertEquals(address.getPhonenumber() ,"0000000000");
		assertNotNull(address.getRegistrationDate());
		assertEquals(address.getRegistrationDate(), date);
		assertEquals(address.getEmail(), "peter.muster@gmail.com");
	}
	
	@Test
	public void testAddressWithSpacedPhoneNumberAndDate() {
		Date date = new Date();
		Address address = new Address(1, "Peter", "Muster", "000 000 00 00", date, "peter.muster@gmail.com");
		assertEquals(address.getId(), 1);
		assertEquals(address.getFirstname(), "Peter");
		assertEquals(address.getLastname(), "Muster");
		assertEquals(address.getPhonenumber(), "000 000 00 00");
		assertNotNull(address.getRegistrationDate());
		assertEquals(address.getRegistrationDate(), date);
		assertEquals(address.getEmail(), "peter.muster@gmail.com");
	}
	
	@Test
	public void testAddAdress(){
		AddressService addressService = new AddressService();
		addressService.setAddressDAO(new AddressDAO_Memory());
		Date date = new Date();
		Address dummyAddress = new Address(1, "Peter", "Muster", "000 000 00 00", date, "peter.muster@gmail.com");
		
		assertEquals(addressService.getAllAddresses().size(), 0);
		
		addressService.registerAddress(dummyAddress);
		
		assertEquals(addressService.getAllAddresses().size(), 1);		
		Address address = addressService.getAllAddresses().get(0);
		
		assertEquals(address.getFirstname(), "Peter");
		assertEquals(address.getLastname(), "Muster");
		assertEquals(address.getPhonenumber(), "000 000 00 00");
		assertNotNull(address.getRegistrationDate());
		assertEquals(address.getEmail(), "peter.muster@gmail.com");
		
		Address editedAddress = new Address(address.getId(),"Max", "Hans", "0000000000", date, "max.hans@gmail.com");
		
		addressService.updateAddress(editedAddress);		
		address = addressService.getAllAddresses().get(0);
		
		assertEquals(address.getFirstname(), "Max");
		assertEquals(address.getLastname(), "Hans");
		assertEquals(address.getPhonenumber(), "0000000000");
		assertNotNull(address.getRegistrationDate());
		assertEquals(address.getEmail(), "max.hans@gmail.com");
		
		addressService.deleteAddress(address.getId());		
		assertTrue(addressService.getAllAddresses().size() == 0);
		
	}
	
	@Test
	public void testValidation(){
		AddressValidator validatior = new AddressValidator();
		
		assertTrue(validatior.isMailValid("peter.muster@gmail.com"));
		assertFalse(validatior.isMailValid("peter.muster-gmail.com"));
		
		assertTrue(validatior.isPhoneNumberValid("000 000 00 00"));
		assertFalse(validatior.isPhoneNumberValid("00000000"));
		assertTrue(validatior.isPhoneNumberValid("0000000000"));
	}
	
	@Test
	public void testEditAddress(){
		AddressDAO_Mockup addressDAO_Mockup = new AddressDAO_Mockup(false);
		AddressService addressService = new AddressService();	
		Date date = new Date();
		
		addressService.setAddressDAO(addressDAO_Mockup);
		
		for(int i = 0; i < addressService.getAllAddresses().size() - 1; i++){
			addressDAO_Mockup.delete(addressService.getAllAddresses().get(i).getId());			
		}
		
		addressDAO_Mockup.create(new Address(1, "Peter", "Muster", "000 000 00 00", date, "peter.muster@gmail.com"));	
		addressService.setAddressDAO(addressDAO_Mockup);
		
		List<Address> tempAddresses = addressService.getAllAddresses();
		List<Address> updatedAddresses = new ArrayList<Address>();
		
		for(Address address : tempAddresses){
			assertEquals(address.getFirstname(), "Peter");
			assertEquals(address.getLastname(), "Muster");
			assertEquals(address.getPhonenumber(), "000 000 00 00");
			assertNotNull(address.getRegistrationDate());
			assertEquals(address.getEmail(), "peter.muster@gmail.com");
			
			updatedAddresses.add(new Address(address.getId(), "Max", "Hans", "0000000000", date, "max.hans@gmail.com"));
		}

		for(Address address : updatedAddresses){
			addressDAO_Mockup.update(address);
		}
		
		addressService.setAddressDAO(addressDAO_Mockup);
		tempAddresses = addressService.getAllAddresses();
		
		for(Address address : tempAddresses){
			assertEquals(address.getFirstname(), "Max");
			assertEquals(address.getLastname(), "Hans");
			assertEquals(address.getPhonenumber(), "0000000000");
			assertNotNull(address.getRegistrationDate());
			assertEquals(address.getEmail(), "max.hans@gmail.com");
		}
	}
}