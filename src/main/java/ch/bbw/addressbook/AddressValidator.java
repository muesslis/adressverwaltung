package ch.bbw.addressbook;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

@SessionScoped
@ManagedBean(name="validator")
public class AddressValidator {
	private final String EMAIL_REGEX = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
	private final String PHONENUMBER_REGEX = "(\\d{3})\\s?(\\d{3})\\s?(\\d{2})\\s?(\\d{2})";
	private final String PHONENUMBER_DIGITCOUNT_REGEX = "^\\d{10}$";
	
	public AddressValidator() {}
	
	public void validateEmail(FacesContext context, UIComponent component, Object value) {
		String email = value.toString();
		
	    if (!isMailValid(email)) {
			FacesMessage message = new FacesMessage("Email ungültig");
			context.addMessage(component.getClientId(context),message);
			message.setSeverity(FacesMessage.SEVERITY_WARN);
			throw new ValidatorException(message);	 			
		}	    
	}
	
	public boolean isMailValid(String email){
		Pattern pattern = Pattern.compile(EMAIL_REGEX);
	    Matcher matcher = pattern.matcher(email);
	    
	    if (matcher.find()) {
	    	return true;
	    }
	    
	    return false;
	}
	
	public void validatePhoneNumber(FacesContext context, UIComponent component, Object value) {
		String phoneNumber = value.toString();
		
	    if (!isPhoneNumberValid(phoneNumber)) {	
			FacesMessage message = new FacesMessage("Telefonnummer ungültig");
			context.addMessage(component.getClientId(context),message);
			message.setSeverity(FacesMessage.SEVERITY_WARN);
			throw new ValidatorException(message);	 			
		}
	}

	public boolean isPhoneNumberValid(String phoneNumber){
	    Pattern phoneNumberPattern = Pattern.compile(PHONENUMBER_REGEX);
	    Pattern digitsPatter = Pattern.compile(PHONENUMBER_DIGITCOUNT_REGEX);
	    Matcher phoneNumberMatcher = phoneNumberPattern.matcher(phoneNumber);
	    Matcher digitsMatcher = digitsPatter.matcher(phoneNumber.replace(" ", ""));
	    
	    if (phoneNumberMatcher.find() && digitsMatcher.find()) {
	    	return true;
	    }
	    
	    return false;
	}
}
