package ch.bbw.addressbook;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

@Named("AddressDAO")
@ApplicationScoped
public class AddressDAO_Memory implements AddressDAO {

	private List<Address> addresses = new ArrayList<>();

	// CRUD Commands: Create Read Update Delete
	public void create(Address address) {
		// address.setId(addresses.size() + 1);
		addresses.add(address);
	}

	public Address read(int id) {
		return addresses.get(id - 1);
	}

	public List<Address> readAll() {
		return addresses;
	}

	public void update(Address address) {
		for (Address oldAddress : addresses) {
			if (oldAddress.getId() == address.getId()) {
				oldAddress.setFirstname(address.getFirstname());
				oldAddress.setLastname(address.getLastname());
				oldAddress.setPhonenumber(address.getPhonenumber());
				oldAddress.setEmail(address.getEmail());
				break;
			}
		}
	}

	public void delete(int id) {
		Iterator<Address> iter = addresses.iterator();
		while (iter.hasNext()) {
			Address oldAddress = iter.next();
			if (oldAddress.getId() == id) {
				iter.remove();
				break;
			}
		}
	}

}
