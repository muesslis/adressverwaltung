package ch.bbw.addressbook;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class AddressViewController {

	@Inject
	private AddressService addressService;

	private List<Address> tempAddresses;

	private String firstname;
	private String lastname;
	private String phoneNumber;
	private String email;

	private String message;

	public AddressViewController() {
		message = "";
	}

	private void clearFields() {
		firstname = "";
		lastname = "";
		phoneNumber = "";
		email = "";
	}

	public void saveAddress() {
		Address address = new Address(0, firstname, lastname, phoneNumber, email);
		addressService.registerAddress(address);
		MailSender sender = new MailSender();
		sender.sendMail(address);
		message = "The address was saved successfully.";
		clearFields();
	}

	public void editAddresses() {
		for (Address address : tempAddresses) {
			addressService.updateAddress(address);
		}
		message = "The addresses were edited successfully.";
	}

	public String removeAddress(int addressId) {
		addressService.deleteAddress(addressId);
		message = "The address is removed.";
		return "editAddresses.xhtml?faces-redirect=true";
	}

	public List<Address> getTempAddress() {
		if (tempAddresses == null) {
			tempAddresses = addressService.getAllAddresses() != null ? new ArrayList<>(addressService.getAllAddresses())
					: new ArrayList<>();
		}
		return tempAddresses;
	}

	public void setTempAddress(List<Address> tempAddress) {
		this.tempAddresses = tempAddress;
	}

	public List<Address> getAddresses() {
		return addressService.getAllAddresses();
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
