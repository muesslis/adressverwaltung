package ch.bbw.addressbook;

import java.util.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.mail.*;
import javax.mail.internet.*;

@Named
@ApplicationScoped

public class MailSender {
	public void sendMail(Address address){
		final String username = "zlatan.adressverwaltung@gmail.com";
		final String password = "Adressverwaltung";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("zlatan.adressverwaltung@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(address.getEmail()));
			message.setSubject("Bestätigung Registrierung");
			message.setText("Grüezi " + address.getFirstname() + " " + address.getLastname() + 
					"\n\n Sie haben sich bei unserer Adressverwaltung registriert. \n\n Freundliche Grüsse \n\n Ihre Adressverwaltung");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
