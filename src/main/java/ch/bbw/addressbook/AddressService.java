package ch.bbw.addressbook;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class AddressService {
	
	@Inject
	@Named("AddressDAO")
	private AddressDAO addressDAO;
	
	public List<Address> getAllAddresses() {
		List<Address> addresses = addressDAO.readAll();
		
		addresses.sort(new AwesomeComparator());
		
		return addresses;
	}
	
	public void registerAddress(Address address) {
		address.setRegistrationDate(new Date());
		addressDAO.create(address);
	}
	
	public void updateAddress(Address address){
		addressDAO.update(address);
	}
	
	public void deleteAddress(int id){
		addressDAO.delete(id);
	}

	public void setAddressDAO(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}

}
